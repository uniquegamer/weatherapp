package isakov.com.weathertest.main;

import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

import isakov.com.weathertest.R;
import isakov.com.weathertest.adapters.MyPagerAdapter;
import isakov.com.weathertest.models.TabPagerItem;
import isakov.com.weathertest.ui.BishkekFragment.BishkekFragment;
import isakov.com.weathertest.ui.LondonFragment.LondonFragment;
import isakov.com.weathertest.ui.SydneyFragment.SydneyFragment;

public class MainActivity extends AppCompatActivity {

    private ArrayList<TabPagerItem> mTabs = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTabs.add(new TabPagerItem(getString(R.string.bishkek), new BishkekFragment()));
        mTabs.add(new TabPagerItem(getString(R.string.london), new LondonFragment()));
        mTabs.add(new TabPagerItem(getString(R.string.sydney), new SydneyFragment()));

        initializeUI();
    }

    private void initializeUI() {
        ViewPager viewPager = findViewById(R.id.viewPager);

        viewPager.setOffscreenPageLimit(mTabs.size());
        viewPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager(), mTabs));
        TabLayout tabLayout = findViewById(R.id.tabs);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tabLayout.setElevation(15);
        }

        tabLayout.setupWithViewPager(viewPager);
    }


}
