package isakov.com.weathertest.ui.SydneyFragment;

import isakov.com.weathertest.models.GeneralModel;
import isakov.com.weathertest.ui.Lifecycle;

/**
 * Created by Isakov on 04-Oct-17.
 */

public interface SydneyContract {
    interface View {
        void success(GeneralModel model);
        void error(String msg);
    }

    interface Presenter extends Lifecycle<View> {
        void load(double lat, double lon, String appId);
    }
}
