package isakov.com.weathertest.ui;

/**
 * Created by Isakov on 04-Oct-17.
 */

public interface Lifecycle<V> {
    void bind(V view);
    void unbind();
}
