package isakov.com.weathertest.ui.BishkekFragment;

import android.util.Log;

import isakov.com.weathertest.api.Api;
import isakov.com.weathertest.models.GeneralModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Isakov on 04-Oct-17.
 */

public class BishkekPresenter implements BishkekContract.Presenter{

    private Api service = null;
    private BishkekContract.View view = null;

    public BishkekPresenter(Api service) {
        this.service = service;
    }

    @Override
    public void bind(BishkekContract.View view) {
        this.view = view;
    }

    @Override
    public void unbind() {
        view = null;
    }

    @Override
    public void load(String appId) {
        service.getCityWeather(1528334, appId, "metric").enqueue(
                new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            view.success(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        if (t != null) view.error(t.getMessage());
                    }
                }
        );
    }
}
