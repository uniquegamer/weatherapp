package isakov.com.weathertest.ui.DetailedActivity;

import isakov.com.weathertest.api.Api;
import isakov.com.weathertest.models.ForecastModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Isakov on 04-Oct-17.
 */

public class DetailedPresenter implements DetailedContract.Presenter {
    private Api service;
    private DetailedContract.View view;

    public DetailedPresenter(Api service) {
        this.service = service;
    }

    @Override
    public void bind(DetailedContract.View view) {
        this.view = view;
    }

    @Override
    public void unbind() {
        view = null;
    }

    @Override
    public void load(int id, String appKey) {
        service.getCityForecast(id, 16, appKey, "metric").enqueue(
                new Callback<ForecastModel>() {
                    @Override
                    public void onResponse(Call<ForecastModel> call, Response<ForecastModel> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            view.success(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<ForecastModel> call, Throwable t) {
                        if (t != null) view.error(t.getMessage());
                    }
                }
        );
    }
}
