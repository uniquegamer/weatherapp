package isakov.com.weathertest.ui.DetailedActivity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import isakov.com.weathertest.R;
import isakov.com.weathertest.WeatherApp;
import isakov.com.weathertest.adapters.ForecastAdapter;
import isakov.com.weathertest.models.ForecastModel;
import isakov.com.weathertest.models.GeneralModel;

/**
 * Created by Isakov on 04-Oct-17.
 */

public class DetailedActivity extends AppCompatActivity implements DetailedContract.View {

    private ListView listView;
    private DetailedPresenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();

        initializeUI();

        sendRequest((GeneralModel) getIntent().getParcelableExtra("model"));

    }

    private void sendRequest(GeneralModel model) {
        presenter.load(model.getId(), getString(R.string.API_KEY));
    }

    private void init() {
        presenter = new DetailedPresenter(WeatherApp.get(this).getService());
        presenter.bind(this);
    }

    private void initializeUI() {
        listView = findViewById(R.id.listView);
    }

    @Override
    public void success(ForecastModel model) {
        listView.setAdapter(new ForecastAdapter(this, model.getList()));
    }

    @Override
    public void error(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.unbind();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
}
