package isakov.com.weathertest.ui.SydneyFragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import isakov.com.weathertest.R;
import isakov.com.weathertest.WeatherApp;
import isakov.com.weathertest.models.GeneralModel;
import isakov.com.weathertest.ui.DetailedActivity.DetailedActivity;
import isakov.com.weathertest.utils.Constants;

/**
 * Created by Isakov on 04-Oct-17.
 */

public class SydneyFragment extends Fragment implements SydneyContract.View, View.OnClickListener {

    private SydneyPresenter presenter;

    private TextView tvCity, tvUpdate, tvCurTemp, tvDetails;
    private ImageView imageView;
    private GeneralModel model;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weather, container, false);
        initializeUI(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.load(46.15, -60.18, getString(R.string.API_KEY));
    }

    private void initializeUI(View view) {
        tvCity = view.findViewById(R.id.tvCity);
        tvUpdate = view.findViewById(R.id.tvUpdate);
        imageView = view.findViewById(R.id.imageView);
        tvCurTemp = view.findViewById(R.id.tvCurTemp);
        tvDetails = view.findViewById(R.id.tvDetails);
        view.findViewById(R.id.button).setOnClickListener(this);
    }

    private void init() {
        presenter = new SydneyPresenter(WeatherApp.get(getActivity()).getService());
        presenter.bind(this);
    }

    @Override
    public void success(GeneralModel model) {
        this.model = model;
        tvCity.setText(model.getName()); //empty name
        tvUpdate.setText(String.format(getString(R.string.last_update), DateFormat.getDateTimeInstance().format(new Date(model.getDt() * 1000))));
        tvCurTemp.setText(String.format(getString(R.string.celsius), String.format(Locale.getDefault(), "%.2f", model.getMain().getTemp())));
        tvDetails.setText(model.getWeatherList().get(0).getDescription());
        Picasso.with(getContext())
                .load(String.format(Locale.getDefault(), Constants.WEATHER_ICON, model.getWeatherList().get(0).getIcon()))
                .into(imageView);
    }

    @Override
    public void error(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.unbind();
    }

    @Override
    public void onClick(View view) {
        startActivity(new Intent(getContext(), DetailedActivity.class)
                .putExtra("model", model));
    }
}
