package isakov.com.weathertest.ui.DetailedActivity;

import isakov.com.weathertest.models.ForecastModel;
import isakov.com.weathertest.ui.Lifecycle;

/**
 * Created by Isakov on 04-Oct-17.
 */

public interface DetailedContract {

    interface View {
        void success(ForecastModel model);
        void error(String msg);
    }

    interface Presenter extends Lifecycle<View> {
        void load(int id, String appKey);
    }
}
