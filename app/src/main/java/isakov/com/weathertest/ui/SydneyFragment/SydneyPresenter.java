package isakov.com.weathertest.ui.SydneyFragment;

import isakov.com.weathertest.api.Api;
import isakov.com.weathertest.models.GeneralModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Isakov on 04-Oct-17.
 */

public class SydneyPresenter implements SydneyContract.Presenter {

    private Api service;
    private SydneyContract.View view = null;

    public SydneyPresenter(Api service) {
        this.service = service;
    }

    @Override
    public void bind(SydneyContract.View view) {
        this.view = view;
    }

    @Override
    public void unbind() {
        view = null;
    }

    @Override
    public void load(double lat, double lon, String appId) {
        service.getCityWeatherByLocation(lat, lon, appId, "metric").enqueue(
                new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            view.success(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<GeneralModel> call, Throwable t) {
                        if (t != null) view.error(t.getMessage());
                    }
                }
        );
    }
}
