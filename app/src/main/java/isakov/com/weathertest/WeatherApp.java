package isakov.com.weathertest;

import android.app.Activity;
import android.app.Application;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import isakov.com.weathertest.api.Api;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static isakov.com.weathertest.utils.Constants.BASE_URL;

/**
 * Created by Isakov on 04-Oct-17.
 */

public class WeatherApp extends Application {

    private Api service;

    public static WeatherApp get(Activity activity ) {
        return (WeatherApp) activity.getApplication();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        service = createService();
    }

    public Api getService() {
        return service;
    }

    private Api createService() {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(getClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(Api.class);

    }

    private OkHttpClient getClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .build();
    }

    private final Interceptor interceptor = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request.Builder ongoing = chain
                    .request().newBuilder()
                    .addHeader("Accept", "application/json;versions=1");
            return chain.proceed(ongoing.build());
        }
    };
}
