package isakov.com.weathertest.api;

import android.text.style.ForegroundColorSpan;

import isakov.com.weathertest.models.ForecastModel;
import isakov.com.weathertest.models.GeneralModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static isakov.com.weathertest.utils.Constants.GET_CITY_WEATHER;
import static isakov.com.weathertest.utils.Constants.GET_CITY_FORECAST;

/**
 * Created by Isakov on 24-Sep-17.
 */

public interface Api {

    @GET(GET_CITY_WEATHER)
    Call<GeneralModel> getCityWeather(@Query("id") int id, @Query("appid") String appId, @Query("units") String units);

    @GET(GET_CITY_WEATHER)
    Call<GeneralModel> getCityWeatherByName(@Query("q") String name, @Query("appid") String appId, @Query("units") String units);

    @GET(GET_CITY_WEATHER)
    Call<GeneralModel> getCityWeatherByLocation(@Query("lat") double lat, @Query("lon") double lon, @Query("appid") String appId, @Query("units") String units);

    @GET(GET_CITY_FORECAST)
    Call<ForecastModel> getCityForecast(@Query("id") int id, @Query("cnt") int cnt, @Query("appid") String appId, @Query("units") String units);

}
